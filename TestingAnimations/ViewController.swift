//
//  ViewController.swift
//  TestingAnimations
//
//  Created by formador on 2/4/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        let coloredLayer = CALayer()
       
        coloredLayer.name = "coloredLayer"
        
        coloredLayer.frame = CGRect(x: 100, y: 100, width: 50, height: 10)
        coloredLayer.backgroundColor = UIColor.green.cgColor
        
        view.layer.addSublayer(coloredLayer)
    }

    @IBAction func moveLayerButtonAction(_ sender: Any) {
        
        let layer = view.layer
        let coloredLayer = layer.sublayers!.first { layer -> Bool in
            layer.name == "coloredLayer"
        }
        
        CATransaction.setAnimationDuration(3)
        
        coloredLayer?.position = CGPoint(x: 100, y: 400)
        
    }
    @IBAction func backButtonAction(_ sender: Any) {
        
        let layer = view.layer
        let coloredLayer = layer.sublayers!.first { layer -> Bool in
            layer.name == "coloredLayer"
        }
        
        CATransaction.begin()
        
        CATransaction.setAnimationDuration(4)
        coloredLayer?.position = CGPoint(x: 100, y: 100)

        
        CATransaction.begin()
        
        CATransaction.setAnimationDuration(1.5)

        coloredLayer?.bounds = CGRect(x: 0, y: 0, width: 120, height: 40)
        
        CATransaction.commit()
        CATransaction.commit()
        
    }
    @IBAction func changeColorButtonAction(_ sender: Any) {
        
        let layer = view.layer
        let coloredLayer = layer.sublayers!.first { layer -> Bool in
            layer.name == "coloredLayer"
        }
        
        
        let startPosition = coloredLayer?.position
        //CAmbiando el modelo
        coloredLayer?.position = CGPoint(x: 100, y: 300)
        coloredLayer?.backgroundColor = UIColor.red.cgColor

        let positionAnimation = CABasicAnimation(keyPath: "position")

        positionAnimation.duration = 3
        positionAnimation.fromValue = startPosition
        positionAnimation.toValue = CGPoint(x: 100, y: 300)
        
        //Added animation
        coloredLayer?.add(positionAnimation, forKey: "position")

        let changeColorAnimation = CABasicAnimation(keyPath: "backgroundColor")

        changeColorAnimation.duration = 3
        changeColorAnimation.fromValue = UIColor.green.cgColor
        changeColorAnimation.toValue = UIColor.red.cgColor
        changeColorAnimation.fillMode = CAMediaTimingFillMode.backwards

        changeColorAnimation.beginTime = CACurrentMediaTime() + 2; //0.3 seconds

        //Added animation
        coloredLayer?.add(changeColorAnimation, forKey: "backgroundColor")
        

    }
}

